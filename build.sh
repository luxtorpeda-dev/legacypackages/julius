#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build julius
#
pushd "source"
mkdir -p build
cd build
cmake ..
make -j "$(nproc)"
popd

cp -rfv "source/build/julius" "517790/dist/julius"
